package mindlabz.com.mindlabz.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mindlabz.com.mindlabz.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
