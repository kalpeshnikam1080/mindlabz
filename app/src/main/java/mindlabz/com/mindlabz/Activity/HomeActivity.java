package mindlabz.com.mindlabz.Activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import mindlabz.com.mindlabz.Fragment.BatchFragment;
import mindlabz.com.mindlabz.Fragment.HomeFragment;
import mindlabz.com.mindlabz.Fragment.SearchFragment;
import mindlabz.com.mindlabz.R;

public class HomeActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private BottomNavigationView bottomNavigationMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("Be Wise|Be Smart|Be Mindlabz");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.navigation_drawer_open,R.string.navigation_drawer_close);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bottomNavigationMenu=findViewById(R.id.bottom_menu);
        bottomNavigationMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment=null;
                switch (item.getItemId())
                {
                    case R.id.bottom_menu_home:
                        fragment=new HomeFragment();
                        break;
                    case R.id.bottom_menu_search:
                        fragment=new SearchFragment();
                        break;
                    case R.id.bottom_menu_batch:
                        fragment=new BatchFragment();
                        break;
                }
                FragmentTransaction fragmentManager=getSupportFragmentManager().beginTransaction();
                fragmentManager.replace(R.id.home_fragment_container,fragment);
                fragmentManager.commit();
                return false;
            }
        });

        FragmentTransaction fragmentManager=getSupportFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.home_fragment_container,new HomeFragment());
        fragmentManager.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
