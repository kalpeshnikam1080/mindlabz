package mindlabz.com.mindlabz.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import mindlabz.com.mindlabz.Class.CourseList;
import mindlabz.com.mindlabz.R;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {

    List<CourseList> courseAdapters;
    Context context;

    public CourseAdapter(Context context,List<CourseList> courseAdapters){
        this.courseAdapters=courseAdapters;
        this.context=context;
    }

    @NonNull
    @Override
    public CourseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.layout_home_course,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseAdapter.ViewHolder holder, int position) {

        Glide.with(context).load(courseAdapters.get(position).getCourse_image()).into(holder.course_image);
        holder.course_name.setText(courseAdapters.get(position).getCourse_name());
    }

    @Override
    public int getItemCount() {
        return courseAdapters.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView course_image;
        TextView course_name;

        public ViewHolder(View itemView) {
            super(itemView);

            course_image=itemView.findViewById(R.id.home_course_image);
            course_name=itemView.findViewById(R.id.home_course_name);
        }
    }
}
