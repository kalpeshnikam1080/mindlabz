package mindlabz.com.mindlabz.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import mindlabz.com.mindlabz.Class.header_image;
import mindlabz.com.mindlabz.R;

public class HeaderImagesAdapter extends RecyclerView.Adapter<HeaderImagesAdapter.ViewHolder> {
    List<header_image> header_list;
    Context context;

    public HeaderImagesAdapter(Context context,List<header_image> header_list)
    {
        this.context=context;
        this.header_list=header_list;
    }

    @NonNull
    @Override
    public HeaderImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.layout_home_header,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HeaderImagesAdapter.ViewHolder holder, int position) {

        Glide.with(context).load(header_list.get(position).getImage_address()).into(holder.headerimage);
    }

    @Override
    public int getItemCount() {
        return header_list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView headerimage;
        public ViewHolder(View itemView) {
            super(itemView);

            headerimage=itemView.findViewById(R.id.home_header_image);
        }
    }
}
