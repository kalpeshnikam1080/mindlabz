package mindlabz.com.mindlabz.Class;

public class CourseList {

    String course_name,course_image;

    public CourseList()
    {

    }

    public CourseList(String course_name, String course_image)
    {
        this.course_name=course_name;
        this.course_image=course_image;
    }

    public String getCourse_image() {
        return course_image;
    }

    public void setCourse_image(String course_image) {
        this.course_image = course_image;
    }

    public String getCourse_name() {

        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }
}
