package mindlabz.com.mindlabz.Class;

public class header_image {

    String image_address;

    public header_image()
    {

    }

    public header_image(String image_address)
    {
        this.image_address=image_address;
    }

    public String getImage_address() {
        return image_address;
    }

    public void setImage_address(String image_address) {
        this.image_address = image_address;
    }
}
