package mindlabz.com.mindlabz.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import mindlabz.com.mindlabz.Adapters.CourseAdapter;
import mindlabz.com.mindlabz.Adapters.HeaderImagesAdapter;
import mindlabz.com.mindlabz.Class.CourseList;
import mindlabz.com.mindlabz.Class.header_image;
import mindlabz.com.mindlabz.R;

public class HomeFragment extends Fragment {

    private RecyclerView header_recycle,course_recycle;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<CourseList> courseLists;
    private ArrayList<header_image> headerList;
    private DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        course_recycle=view.findViewById(R.id.home_course_recycle);
        header_recycle=view.findViewById(R.id.home_header_recycle);

        courseLists=new ArrayList<>();
        CourseList list=new CourseList("Java","https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2F253484_8f2e_6.jpg?alt=media&token=5326e4d7-0133-4b8f-acc7-ce80d17addf7");
        CourseList list1=new CourseList("Firebase","https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Ffirebase.png?alt=media&token=c2a6a6b7-c436-45c2-9a0c-c8be4e2822e6");
        CourseList list2=new CourseList("Node","https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Fnode.png?alt=media&token=11a9c934-12a4-4fa4-bbef-83b18afb643e");
        CourseList list3=new CourseList("Google Cloud","https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Fgoogle.png?alt=media&token=1be04f76-4c95-4faf-b8d0-c8a0d8addefe");
        courseLists.add(list);
        courseLists.add(list1);
        courseLists.add(list2);
        courseLists.add(list3);
        courseLists.add(list2);
        courseLists.add(list1);
        courseLists.add(list3);
        course_recycle.setLayoutManager(new GridLayoutManager(getContext(),2));
        CourseAdapter courseAdapter=new CourseAdapter(getContext(),courseLists);
        course_recycle.setAdapter(courseAdapter);

        headerList=new ArrayList<>();
        header_image header_images=new header_image("https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2F253484_8f2e_6.jpg?alt=media&token=5326e4d7-0133-4b8f-acc7-ce80d17addf7");
        header_image header_images1=new header_image("https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Ffirebase.png?alt=media&token=c2a6a6b7-c436-45c2-9a0c-c8be4e2822e6");
        header_image header_images2=new header_image("https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Fnode.png?alt=media&token=11a9c934-12a4-4fa4-bbef-83b18afb643e");
        header_image header_images3=new header_image("https://firebasestorage.googleapis.com/v0/b/mindlabz-6f809.appspot.com/o/Courses%2Fgoogle.png?alt=media&token=1be04f76-4c95-4faf-b8d0-c8a0d8addefe");
        headerList.add(header_images);
        headerList.add(header_images1);
        headerList.add(header_images2);
        headerList.add(header_images3);

        header_recycle.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        HeaderImagesAdapter imagesAdapter=new HeaderImagesAdapter(getContext(),headerList);
        header_recycle.setAdapter(imagesAdapter);

        return view;
    }
}
